# FabricJS Canvas Editor

This is a canvas editor that I created with the amazing FabricJS library.

I wanted to have something that I could use to quickly create images by canvas. This example project will 
allow you to add images and text to a canvas. Then you can take that canvas and preview it as an image.

The canvas layers will be shown that you can move up and down. There are also numerous properties you can set for each image and text object.