var canvas = new fabric.Canvas('c');
		
//canvas.selectionColor = 'rgba(0,255,0,0.3)';
//canvas.selectionBorderColor = 'red';
//canvas.selectionLineWidth = 5;

function resizeCanvas() 
{
	canvas.setWidth($("#canvas-section").width());
	canvas.setHeight(parseInt($("#canvas-section").width() / 1.77));
	canvas.renderAll();
}

resizeCanvas();

/*canvas.setBackgroundColor({source: 'img-placeholder.png', repeat: 'repeat'}, function () {
  canvas.renderAll();
});*/

function setCanvasColor()
{
	canvas.setBackgroundColor($('#canvas-color').val(), canvas.renderAll.bind(canvas));
}

function removeCanvasColor()
{
	canvas.setBackgroundColor(null, canvas.renderAll.bind(canvas));
}

canvas.on('mouse:down',function(event)
{
	showProperties();
});

window.addEventListener('resize', resizeCanvas, false);

function selectCanvasObject(index)
{
	canvas.setActiveObject(canvas.item(index));
	canvas.renderAll();
	
	showProperties();
}

function reloadObjectList()
{
	$("#canvas-object-list").html('');
	//debugger;
	
	for(var i = (canvas.getObjects().length - 1);i > -1;i--)
	{
		var objectLink = "<div class='p-grid' style='border-bottom: dashed 1px #000000'>";
		
		if(canvas.item(i)._element)
		{
			objectLink += "<div class='p-col-3 p-md-3'>";
			objectLink += "<a href='#' onclick='selectCanvasObject("+ i +");return false;' style='color: black;display: block'>";
			objectLink += "<img src='" + canvas.item(i).getSrc() + "' style='width: 100%'/>";
			objectLink += "</a></div><div class='p-col-7 p-md-7'>";
			objectLink += "<a href='#' onclick='selectCanvasObject("+ i +");return false;' style='color: black;display: block'>";
			objectLink += canvas.item(i)._element.className + "</a></div>";
		}
		
		if(canvas.item(i)._text)
		{
			objectLink += "<div class='p-col-2 p-md-2'>";
			objectLink += "<a href='#' onclick='selectCanvasObject("+ i +");return false;' style='color: black;display: block'>";
			objectLink += "<img src='./text2.png' style='width: 100%'/>";
			objectLink += "</a></div><div class='p-col-8 p-md-8'>";
			objectLink += "<a href='#' onclick='selectCanvasObject("+ i +");return false;' style='color: black;display: block'>";
			objectLink += canvas.item(i).text + "</a></div>";
		}
		
		objectLink += "<div class='p-col-2 p-md-2'>";
		
		if(i != (canvas.getObjects().length - 1))
		{
			objectLink += "<a href='#' style='margin-left: 8px;color: black' onclick='sendObjectToFront(" + i + ");return false;'><i class='fas fa-angle-double-up' style='font-size: 24px;'></i></a>";
			
			objectLink += "<a href='#' style='margin-left: 8px;color: black' onclick='sendObjectUpOne(" + i + ");return false;'><i class='fas fa-angle-up' style='font-size: 24px;'></i></a>";
		}
		
		if(i != 0)
		{
			objectLink += "<a href='#' style='margin-left: 8px;color: black' onclick='sendObjectDownOne(" + i + ");return false;'><i class='fas fa-angle-down' style='font-size: 24px;'></i></a>";
		
			objectLink += "<a href='#' style='margin-left: 8px;color: black' onclick='sendObjectToBack(" + i + ");return false;'><i class='fas fa-angle-double-down' style='font-size: 24px;'></i></a>";
		}
		
		objectLink += "</div></div>";
		
		$("#canvas-object-list").append(objectLink);
	}
	
	if(canvas.getObjects().length < 1)
	{
		$("#layers-title").hide();
	}
	else
	{
		$("#layers-title").show();
	}
}

function showProperties()
{
	if(canvas.getActiveObject())
	{
		$("#object-properties").show();
		
		$("#opacity").val(canvas.getActiveObject().get('opacity'));
		
		//alert(event.target);
		if(canvas.getActiveObject()._text)
		{
			populateTextPropertyFields();
			
			$(".txtProperties").show();
			$(".imgProperties").hide();
		}
		
		if(canvas.getActiveObject()._element)
		{
			populateImagePropertyFields();
			
			$(".txtProperties").hide();
			$(".imgProperties").show();
		}
	}
	else
	{	
		$("#object-properties").hide();
		$(".txtProperties").hide();
		$(".imgProperties").hide();
	}
}

function sendObjectDownOne(index)
{
	var obj = canvas.item(index);
	
	obj.sendBackwards();
	reloadObjectList();
	canvas.renderAll();
}

function sendObjectToBack(index)
{
	var obj = canvas.item(index);
	
	obj.sendToBack();
	reloadObjectList();
	canvas.renderAll();
}

function sendObjectUpOne(index)
{
	var obj = canvas.item(index);
	
	obj.bringForward();
	reloadObjectList();
	canvas.renderAll();
}

function sendObjectToFront(index)
{
	var obj = canvas.item(index);
	
	obj.bringToFront();
	reloadObjectList();
	canvas.renderAll();
}

function deleteObject()
{
	canvas.remove(canvas.getActiveObject());
	canvas.renderAll();
	showProperties();
	reloadObjectList();
}

function setOpacity()
{
	canvas.getActiveObject().set('opacity', $("#opacity").val());
	canvas.renderAll();
}

function hexToRgb(hex) 
{
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	
	hex = hex.replace(shorthandRegex, function(m, r, g, b) {
		return r + r + g + g + b + b;
	});

	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

function rgbToHex(r, g, b) 
{
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}