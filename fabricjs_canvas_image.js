document.getElementById('file').addEventListener("change", function (e) 
{
	var file = e.target.files[0];
	var reader = new FileReader();
	
	reader.onload = function (f) 
	{
		var data = f.target.result;                    
		
		fabric.Image.fromURL(data, function (img) 
		{
			var oImg = img.set({left: 0, top: 0, angle: 0, transparentCorners: false});
			oImg['borderColor'] = 'black';
			oImg['cornerColor'] = 'black';
			oImg['cornerStrokeColor'] = 'black';
			oImg.scale(0.5);
			
			canvas.add(oImg);
			canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
			canvas.renderAll();
			
			reloadObjectList();
			
			
			$("#brownie").prop("checked", false);
			$("#vintage").prop("checked", false);
			$("#technicolor").prop("checked", false);
			$("#polaroid").prop("checked", false);
			$("#kodachrome").prop("checked", false);
			$("#blackwhite").prop("checked", false);
			$("#grayscale").prop("checked", false);
			$("#invert").prop("checked", false);
			$("#sepia").prop("checked", false);
			$("#brightness").prop("checked", false);
			$("#brightness-value").val(0.1);
			$("#contrast").prop("checked", false);
			$("#contrast-value").val(0);
			$("#saturation").prop("checked", false);
			$("#saturation-value").val(0);
			$("#noise").prop("checked", false);
			$("#noise-value").val(100);
			$("#pixelate").prop("checked", false);
			$("#pixelate-value").val(4);
			$("#blur").prop("checked", false);
			$("#blur-value").val(0.1);
			$("#sharpen").prop("checked", false);
			$("#emboss").prop("checked", false);
			$("#hue").prop("checked", false);
			$("#hue-value").val(0);
			
			$("#object-properties").show();
			$(".imgProperties").show();
			$(".txtProperties").hide();
			//var dataURL = canvas.toDataURL({format: 'png', quality: 0.8});
		});
	};
	
	reader.readAsDataURL(file);
	
	$('#file').val('');
});

function populateImagePropertyFields()
{
	var imgObject = canvas.getActiveObject();
				
	if(imgObject.filters[4])
	{
		$("#brownie").prop("checked", true);
	}
	else
	{
		$("#brownie").prop("checked", false);
	}
	
	if(imgObject.filters[9])
	{
		$("#vintage").prop("checked", true);
	}
	else
	{
		$("#vintage").prop("checked", false);
	}
	
	if(imgObject.filters[14])
	{
		$("#technicolor").prop("checked", true);
	}
	else
	{
		$("#technicolor").prop("checked", false);
	}
	
	if(imgObject.filters[15])
	{
		$("#polaroid").prop("checked", true);
	}
	else
	{
		$("#polaroid").prop("checked", false);
	}
	
	if(imgObject.filters[18])
	{
		$("#kodachrome").prop("checked", true);
	}
	else
	{
		$("#kodachrome").prop("checked", false);
	}
	
	if(imgObject.filters[19])
	{
		$("#blackwhite").prop("checked", true);
	}
	else
	{
		$("#blackwhite").prop("checked", false);
	}
	
	if(imgObject.filters[0])
	{
		$("#grayscale").prop("checked", true);
	}
	else
	{
		$("#grayscale").prop("checked", false);
	}
	
	if(imgObject.filters[1])
	{
		$("#invert").prop("checked", true);
	}
	else
	{
		$("#invert").prop("checked", false);
	}
	
	if(imgObject.filters[3])
	{
		$("#sepia").prop("checked", true);
	}
	else
	{
		$("#sepia").prop("checked", false);
	}
	
	if(imgObject.filters[5])
	{
		$("#brightness").prop("checked", true);
		$("#brightness-value").val(imgObject.filters[5].brightness);
	}
	else
	{
		$("#brightness").prop("checked", false);
		$("#brightness-value").val(0.1);
	}
	
	if(imgObject.filters[6])
	{
		$("#contrast").prop("checked", true);
		$("#contrast-value").val(imgObject.filters[6].contrast);
	}
	else
	{
		$("#contrast").prop("checked", false);
		$("#contrast-value").val(0);
	}
	
	if(imgObject.filters[7])
	{
		$("#saturation").prop("checked", true);
		$("#saturation-value").val(imgObject.filters[7].saturation);
	}
	else
	{
		$("#saturation").prop("checked", false);
		$("#saturation-value").val(0);
	}
	
	if(imgObject.filters[8])
	{
		$("#noise").prop("checked", true);
		$("#noise-value").val(imgObject.filters[8].noise);
	}
	else
	{
		$("#noise").prop("checked", false);
		$("#noise-value").val(100);
	}
	
	if(imgObject.filters[10])
	{
		$("#pixelate").prop("checked", true);
		$("#pixelate-value").val(imgObject.filters[10].blocksize);
	}
	else
	{
		$("#pixelate").prop("checked", false);
		$("#pixelate-value").val(4);
	}
	
	if(imgObject.filters[11])
	{
		$("#blur").prop("checked", true);
		$("#blur-value").val(imgObject.filters[11].blur);
	}
	else
	{
		$("#blur").prop("checked", false);
		$("#blur-value").val(0.1);
	}
	
	if(imgObject.filters[12])
	{
		$("#sharpen").prop("checked", true);
	}
	else
	{
		$("#sharpen").prop("checked", false);
	}
	
	if(imgObject.filters[13])
	{
		$("#emboss").prop("checked", true);
	}
	else
	{
		$("#emboss").prop("checked", false);
	}
	
	if(imgObject.filters[21])
	{
		$("#hue").prop("checked", true);
		$("#hue-value").val(imgObject.filters[21].rotation);
	}
	else
	{
		$("#hue").prop("checked", false);
		$("#hue-value").val(0);
	}
}

f = fabric.Image.filters;

function applyFilter(index, filter) 
{
	var obj = canvas.getActiveObject();
	obj.filters[index] = filter;
	obj.applyFilters();
	
	canvas.renderAll();
}

function getFilter(index) 
{
	var obj = canvas.getActiveObject();
	return obj.filters[index];
}

function applyFilterValue(index, prop, value) 
{
	var obj = canvas.getActiveObject();

	if (obj.filters[index]) 
	{
		obj.filters[index][prop] = value;
		obj.applyFilters();
		canvas.renderAll();
	}
}

$('#brownie').click(function() {
	applyFilter(4, $(this).is(":checked") && new f.Brownie());
});
  
$('#vintage').click(function() {
	applyFilter(9, $(this).is(":checked") && new f.Vintage());
});
  
$('#technicolor').click(function() {
	applyFilter(14, $(this).is(":checked") && new f.Technicolor());
});
  
$('#polaroid').click(function() {
	applyFilter(15, $(this).is(":checked") && new f.Polaroid());
});
  
$('#kodachrome').click(function() {
	applyFilter(18, $(this).is(":checked") && new f.Kodachrome());
});
  
$('#blackwhite').click(function() {
	applyFilter(19, $(this).is(":checked") && new f.BlackWhite());
});
  
$('#grayscale').click(function() {
	applyFilter(0, $(this).is(":checked") && new f.Grayscale());
});

$('#invert').click(function() {
	applyFilter(1, $(this).is(":checked") && new f.Invert());
});

$('#sepia').click(function() {
	applyFilter(3, $(this).is(":checked") && new f.Sepia());
});

$('#brightness').click(function () {
	applyFilter(5, $(this).is(":checked") && new f.Brightness({
	  brightness: parseFloat($('#brightness-value').val())
	}));
});
  
$('#brightness-value').change(function() {
	applyFilterValue(5, 'brightness', parseFloat($(this).val()));
});

$('#contrast').click(function () {
	applyFilter(6, $(this).is(":checked") && new f.Contrast({
	  contrast: parseFloat($('#contrast-value').val())
	}));
});

$('#contrast-value').change(function() {
	applyFilterValue(6, 'contrast', parseFloat($(this).val()));
});
  
$('#saturation').click(function () {
	applyFilter(7, $(this).is(":checked") && new f.Saturation({
	  saturation: parseFloat($('#saturation-value').val())
	}));
});
  
$('#saturation-value').change(function() {
	applyFilterValue(7, 'saturation', parseFloat($(this).val()));
});
  
$('#noise').click(function () {
	applyFilter(8, $(this).is(":checked") && new f.Noise({
	  noise: parseInt($('#noise-value').val(), 10)
	}));
});
  
$('#noise-value').change(function() {
	applyFilterValue(8, 'noise', parseInt($(this).val(), 10));
});
  
$('#pixelate').click(function() {
	applyFilter(10, $(this).is(":checked") && new f.Pixelate({
	  blocksize: parseInt($('#pixelate-value').val(), 10)
	}));
});
  
$('#pixelate-value').change(function() {
	applyFilterValue(10, 'blocksize', parseInt($(this).val(), 10));
});
  
$('#blur').click(function() {
	applyFilter(11, $(this).is(":checked") && new f.Blur({
	  value: parseFloat($('#blur-value').val())
	}));
});
  
$('#blur-value').change(function() {
	applyFilterValue(11, 'blur', parseFloat($(this).val(), 10));
});
  
$('#sharpen').click(function() {
	applyFilter(12, $(this).is(":checked") && new f.Convolute({
	  matrix: [  0, -1,  0,
				-1,  5, -1,
				 0, -1,  0 ]
	}));
});
  
$('#emboss').click(function() {
	applyFilter(13, $(this).is(":checked") && new f.Convolute({
	  matrix: [ 1,   1,  1,
				1, 0.7, -1,
			   -1,  -1, -1 ]
	}));
});

$('#hue').click(function() {
	applyFilter(21, $(this).is(":checked") && new f.HueRotation({
	  rotation: $('#hue-value').val(),
	}));
});

$('#hue-value').change(function() {
	applyFilterValue(21, 'rotation', $(this).val());
});